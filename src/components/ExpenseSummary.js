import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import numeral from 'numeral';
import selectExpense from '../selectors/expenses';
import selectExpenseTotal from '../selectors/expenses-total';
import getVisibleExpenses from '../selectors/expenses';


export const ExpensesSummary = ({ expenseCount, expensesTotal }) => {
  const expenseWord = expenseCount === 1 ? 'expense' : 'expenses';
  const formattedExpensesTotal = numeral(expensesTotal / 100).format('$0,0.00');

  return (
    <div className="page-header">
      <div className="content-container">
        <h1 className="page-header__title">
          Viewing <span>{expenseCount}</span> {expenseWord} totalling <span>{formattedExpensesTotal}</span>
        </h1>
        <div className="page-header__actions">
          <Link className="button" to="/create">Add Expense</Link>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  const visbleExpenses = selectExpense(state.expenses, state.filters);

  return {
    expenseCount: getVisibleExpenses.length,
    expensesTotal: selectExpenseTotal(visbleExpenses),
  };
}

export default connect(mapStateToProps)(ExpensesSummary);