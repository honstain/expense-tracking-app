import * as firebase from 'firebase';
//import expenses from '../tests/fixtures/expenses';

const config = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.FIREBASE_APP_ID,
};

// Initialize Firebase
firebase.initializeApp(config);

const database = firebase.database();
const googleProvider = new firebase.auth.GoogleAuthProvider();

export { firebase, database, googleProvider };

/**
 * ************************************************************************
 * EXPERIMENTING WITH FIREBASE QUERIES
 * ************************************************************************
 *
 */

// const notes = [
//   {id: 12, title: 'First note'},
//   {id: 13, title: 'First note'},
// ];

// expenses.map((exp) => {
//   database.ref('expenses').push(exp);
// });
// database.ref('notes').push(notes[1]);

//database.ref('notes/-LkR3TH9zYyLEMuR_eQq').update({ id: 15 });

// database.ref('expenses').on('child_removed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val());
// });

// database.ref('expenses').on('child_changed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val());
// });

// database.ref('expenses').on('child_added', (snapshot) => {
//   console.log(snapshot.key, snapshot.val());
// });



// database.ref('expenses')
//   .once('value')
//   .then((snapshot) => {
//     const expenses = [];

//     snapshot.forEach((childSnapshot) => {
//       expenses.push({
//         id: childSnapshot.key,
//         ...childSnapshot.val()
//       });
//     });
//     console.log(expenses);
//   });

// const onValueChange = database.ref('expenses')
//   .on('value', (snapshot) => {
//     const expenses = [];

//     snapshot.forEach((childSnapshot) => {
//       expenses.push({
//         id: childSnapshot.key,
//         ...childSnapshot.val()
//       });
//     });
//     console.log('live update', expenses);
//   }, (error) => {
//     console.log('live update error', error);
//   });

// console.log('calling firebase');
// firebase.database().ref().set({
//   name: 'Andrew Mead',
//   age: 26,
//   isSingle: true,
//   location: {
//     city: 'Philadelphia',
//     country: 'United States'
//   }
// }).then(() => {
//   console.log('Data is saved');
// }).catch((error) => {
//   console.log('Firebase DB call failed', error)
// });;

// database.ref().update({
//   name: 'Mike'
// });

// database.ref('isSingle')
//   .remove()
//   .then(() => {
//     console.log('Remove success');
//   }).catch((error) => {
//     console.log('Remove failure', error);
//   });


// Run it a single time
// database.ref()
//   .once('value')
//   .then((snapshot) => {
//     const val = snapshot.val();
//     console.log('single select', val);
//   })
//   .catch((error) => {
//     console.log('error', error);
//   });

// const onValueChange = database.ref('location')
//   .on('value', (snapshot) => {
//     console.log('live update', snapshot.val());
//   }, (error) => {
//     console.log('live update error', error);
//   });

// setTimeout(() => {
//   console.log('Disable live update');
//   database.ref('location').off('value', onValueChange);
// }, 5000);