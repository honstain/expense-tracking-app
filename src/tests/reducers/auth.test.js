import authReducer from '../../reducers/auth';

test('should set default state', () => {
  const state = authReducer(undefined, { type: '@@INIT' });
  expect(state).toEqual({});
});

test('should set uid for LOGIN', () => {
  const uid = 'A123';
  const state = authReducer({}, { type: 'LOGIN', uid });
  expect(state).toEqual({ uid });
});

test('should clear state for LOGOUT', () => {
  const uid = 'A123';
  const state = authReducer({ uid }, { type: 'LOGOUT' });
  expect(state).toEqual({});
});
