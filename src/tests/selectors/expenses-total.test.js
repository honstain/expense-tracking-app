import React from 'react';
import selectExpenseTotal from '../../selectors/expenses-total'
import expenses from '../fixtures/expenses';

test('expense list total for no expenses', () => {
  expect(selectExpenseTotal([])).toBe(0);
});

test('expense list total for single expenses', () => {
  expect(selectExpenseTotal([expenses[0]])).toBe(195);
});

test('expense list total for several expenses', () => {
  expect(selectExpenseTotal(expenses)).toBe(114195);
});